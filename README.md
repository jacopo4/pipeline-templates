# Nano Pipelines
Nano Solutions Private Gitlab Pipelines

# Pipeline Stages

## Preparation 
[composer, npm, fetch_dockerfiles]


## Build
[build_assets, db_seeding]

```yaml
# use this to set class name of the root seeder when running db_seeding if required
DB_SEEDER: "--seeder=TestingSeeder"
# use this to set class name of the root seeder when running db_seeding_percy if required
DB_SEEDER_PERCY: "--seeder=PercySeeder"
```



## Tests 
[features, browser_mobile, browser_desktop, browser_tablet, codestyle, security-checker, npmaudit, phpcpd, cypress, cypress-percy, feature-percy]

```yaml
# If set to 1 runs cypress, else runs all browser test (mobile, tablet, desktop)
RUN_CYPRESS: 1 # 0 runs dusk 
RUN_DUSK_ALL_BROWSER_JOBS: 0 # default only tablet
RUN_UNIT_TESTS: 0 # runs unit test if set to 1
SKIP_BROWSER: 1 # disables both dusk and cypress
SKIP_NPM_AUDIT: 1 # disables npm-audit job for projects without npm
SKIP_PERCY: 1 # use it if you wanna run cypress without Percy on master and develop
```


## Docker 
[ build-image ]

```yaml
BUILD_DOCKER: 1 # docker image will not build unless this is set to one
SCAN_DOCKER: 1 # docker scan job does not run on branches unless this is set to one, still runs for master|develop|tags
```



## Deployment
[deployer_deploy_feature, deployer_deploy_staging, deployer_deploy_production, docker_deploy_feature, docker_deploy_staging, docker_deploy_production, kube_deploy_feature, kube_deploy_staging, kube_deploy_production]

```yaml
# Required. Only include host section of the url. no https:// and no trailing slashes
PRODUCTION_HOST: operations.sls.com.au 
STAGING_HOST: operations.nano.rocks

# Required
PRODUCTION_DEPLOYMENT: deployer|docker|kubernetes
STAGING_DEPLOYMENT: deployer|docker|kubernetes
FEATURE_DEPLOYMENT: deployer|kubernetes

# Only required when using DEPLOYER.
#Include full webhook url, including https:// in front.
DEPLOYER_FEATURE_HOOK: 'https://deploy.nano.rocks/deploy/MprNwWYvkgB7ue0T6EljhVf5jy0aQz88LqFw90sV12HQVTytt7sKDfnGH8Kd'
DEPLOYER_STAGING_HOOK: 'https://deploy.nano.rocks/deploy/dUrt12GpvBoqRVvnXkxi2GbZn1MX0sXSJYXdBcp4AlYJ0btrW6n0Hr7aKA3O'
DEPLOYER_PRODUCTION_HOOK: 'https://deploy.nano.rocks/deploy/dUrt12GpvBoqRVvnXkxi2GbZn1MX0sXSJYXdBcp4AlYJ0btrW6n0Hr7aKA3O'

# Only required when using DEPLOYER in development
# Only include host section of url.
DEPLOYER_FEATURE_HOST: operations.nano.town

# Only required when using DOCKER.
# Name of the folder where the deployment files are stored.
# Usually a subfolder of containers.sls.com.au:/srv/
PRODUCTION_DIR: operations-production
STAGING_DIR: operations-staging

# Required: this is used for kubernetes namespaces and for submitting reports to chief.nano.rocks
# This should be set to the name of the project, NEEDS TO BE LOWERCASE!!
PROJECT_DOMAIN: operations

# Set it to 0 to disable deployment of horizon, redis (enabled by default, not necessary if we want horizon, redis to be enabled)
HORIZON: 0
REDIS: 0
```