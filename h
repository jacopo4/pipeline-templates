[33m21ee5aa[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m Dont allow failure for review jobs
[33m37ec206[m[33m ([m[1;31morigin/master[m[33m)[m Try fix blocked pipelines
[33m3efbc79[m yaml error fix
[33mf57a7d0[m Change stage for scan job
[33m7eb3c41[m Fix syntax
[33m934d3cf[m Add image scanning to pipeline
[33m70f7a3e[m Cat sensiolab json
[33m2b409d9[m Only disable npm audit
[33m69db9b8[m Dont run npm jobs if variable is set
[33m6fbf8e4[m Syntax
[33ma563445[m Upload reports as artifacts for debugging
[33mbc9a6fe[m Merge branch 'master' of git.nano.rocks:nano-images/pipelines-template
[33m7ac0531[m Print node and npm version
[33me8551d7[m Set gitlab in helm
[33m5f6f72b[m Wait to deploy
[33mcd61f28[m Try upload multiple reports for cypress
[33m95688aa[m Report for unit tests
[33mef0adde[m Add report to cypress job
[33m7381f7c[m Test
[33m42aa2bc[m Test
[33ma321367[m Test
[33m4223b79[m Do not expire .env artifact
[33me8c229d[m Reports specific names
[33m4dd88b3[m Test reports for tests
[33mc2beb9f[m Test reports for tests
[33m1d189aa[m Docs for new variable
[33m206991f[m Print env
[33mcda33c5[m Syntax
[33m9a5f5c4[m Test
[33m02cabd4[m Fix browser variable
[33md6534c8[m Allow projects to not run any browser test framework
[33m5f1b672[m Test
[33m7f27fb2[m Test
[33mc85a9a9[m Modify kube deployment
[33md37f451[m Fixing deployment jobs for kubernetes
[33m8aa9f3f[m End testing
[33m2b935c7[m Rename
[33m3c3ed12[m Parse version in after script
[33m048629f[m Test if after script can see variable
[33md4a263d[m Post laravel version to chief
[33mfc668d8[m No need for cat anymore
[33ma7f0b6e[m Syntax
[33md0c8c56[m Cat report before sending
[33m02edac6[m Test if npm post request to chief is working
[33me18e0bb[m Test report submission
[33mb96944c[m Remove useless need directive from cypress percy
[33m0c4a702[m Submit report in after script
[33mbd7c855[m Remove template from sensiolabs job
[33madef3de[m Test
[33mdcc0709[m Syntax
[33m1263cd8[m No need to fetch dockerfiles if not building image
[33m84f5765[m Sensiolabs
[33mf9b2caa[m Modify sensiolabs job
[33m80d64c8[m Sensiolabs modify
[33ma285f14[m Remove unnecessary needs
[33m76f98f5[m Test
[33m1080825[m Test
[33mf601820[m Test
[33me63f902[m test
[33mb4eef06[m Syntax
[33m751a67e[m Features need db seeding
[33m99345c7[m Modified php checker job to use different binary
[33m760898d[m Submit reports only from master
[33m23aff28[m Test again
[33m8d4fba6[m Test again
[33m522c520[m Test
[33m4afd1c1[m No need for different sensiolab job
[33m87d3b52[m Save sensio report one folder up
[33mb9ec55a[m Passing arguments to npm audit script
[33mc2ad0be[m Modify sensiolab job
[33m489469c[m Correct syntax
[33m060cc8e[m Added sensiolabs job for production
[33m4da03b7[m Fixed curl request for npm audit job
[33ma6a11b6[m Follow redirects
[33m2d3f47e[m Testing that we can upload to chief
[33m84b3e00[m Test
[33m96c5ca4[m Try upload json file to nano chief
[33mfff09c8[m Remove random file
[33m0a63f3c[m Modify npm audit job
[33m3b5150f[m Running script from local
[33me9a29c2[m Fix yaml syntax, maybe
[33mc77029e[m Skip docker if not needed
[33mb53b053[m Added pipeline for nanochief with no tests
[33mf5c946b[m Fix syntax
[33m8776674[m Fetch from branch
[33mfc4fa8d[m Fetch dockerfiles from master
[33m1ad6dbb[m Pull helm charts master
[33m3c04416[m Correct syntax
[33m84eb381[m Fetch from dev branch
[33m86a4f32[m Fix bugsnag
[33mc1cadc2[m Do not build image for scheduled pipelines
[33m8394f70[m More changes to deployment
[33m0e68f09[m Merge branch 'master' of git.nano.rocks:nano-images/pipelines-template
[33mc49b42c[m Change deployment way of checking if release exists
[33maeeb987[m adding frontend to public
[33m106907f[m More deployment changes
[33m954eb25[m Syntax correction
[33m73c4c7b[m Do not build image if no file has changed
[33maaffb2f[m Do not use helm upgrade
[33m1874974[m Pipeline status should not be blocked
[33m1bb5951[m Dont use needs for review jobs
[33m003e708[m Modified stop jobs
[33m3919d08[m More optional
[33mfea811a[m Allow manual jobs to fail
[33m28c4029[m Configurable redis
[33m0e1c6d7[m Correct syntax
[33m8ff3845[m Pull helm chart from develop
[33me254624[m Disable horizon if not needed
[33md05a63e[m Change app url
[33m115c413[m Fix domains
[33mc26e718[m Changed domain for staging and production kube deployments
[33ma66ecf0[m Add expiry date for artifacts where missing
[33m2bd68cf[m Truing negaivt for phpcdf
[33m351f0dd[m 🐛 FIX: 1 need soemthing
[33mcad058a[m Disable/enable unit tests
[33m7d99c1e[m Fixing syntax
[33mad31853[m Adding unut
[33mf4084fd[m Make kube deployment manual
[33mf15c401[m All jobs back into pipeline
[33ma21c129[m Put rules back for killer jobs
[33m50e1c35[m Right env name
[33m0bb234b[m No rules for killer jobs
[33m795ca28[m Removed when config
[33m8879c89[m Added killer jobs for staging and production
[33mba36a76[m Stop environment job for kubernetes
[33mc4276b5[m Testing
[33m90c8a63[m Testing
[33m99810b0[m Testing
[33m7a146aa[m Testing
[33m6d6e403[m Testing
[33mc9247fb[m Testing
[33m7df9992[m Testing
[33m91aee46[m K8 not deploying to right namespace
[33m929126a[m Dynamic namespace
[33m2894d2d[m Correct namespace syntax
[33m1c2176a[m Modify namespace name
[33m9749d5d[m One namespace to one release
[33m34b2068[m Testing
[33ma19d82f[m Testing
[33mdf44cdd[m Testing
[33md395df0[m Testing
[33m32ed609[m Testing
[33m4cc09cd[m Testing
[33m4210fd8[m Test
[33m17ebae4[m Testing
[33m8470bae[m Test
[33md68b0a2[m Modified kubernetes deployment
[33mf1a9292[m Syntax
[33md274c83[m Modified conditions
[33mf694a73[m Testing
[33m0a417b5[m Lint
[33m7fd8665[m 🐛 FIX: Fix cypress
[33m9a23e9f[m RUN_CYPRESS 0 runs dusk
[33md949776[m Testing dusk switchers
[33mebab3ba[m Revert deployment to his old needs
[33mf586586[m Install cypress binary before running it
[33md56f7d2[m Test
[33m3dad9d5[m Test
[33m44f9277[m Test
[33m989d76e[m More cypress changes
[33m623fb7a[m More cypress changes
[33mf7075af[m Modified cypress
[33m9295080[m Modified cypress
[33mfc1fd1d[m Modified cypress
[33m4c14537[m Implementing Percy
[33md61c73e[m Implementing Percy
[33mb75d540[m Testing
[33m611def9[m Testing
[33m143e7e2[m Testing
[33m309ad81[m Testing
[33m4bf2c6f[m Testing
[33mea4da21[m Testing
[33m147e3a1[m Testing
[33mf30ce57[m Testing
[33mafd8a35[m Testing
[33m2ee106f[m Added conditions for db seeding jobs
[33m5f12c7a[m Trying condition negation
[33mc2e3b65[m No need to switch context anymore
[33m568c078[m Renamed db seeding file
[33m8da888e[m Added rules for db seeding jobs
[33m5ad8c6a[m Added db seeding percy job
[33m88f1664[m Added variable for root seeder in db seeding
[33mb59fa63[m Correct rule
[33m0a623f7[m Added variable to toggle cypress/browser tests
[33mf7d6e90[m Removed backslash as it does not solve problem
[33m0e31899[m Added backslash in test job
[33mf498378[m Added backslash in test job
[33mdc64709[m Use new browser tests template
[33mf9d0601[m Added tag to browser jobs and made template
[33m3c7eeb7[m Enable web pipelines
[33mce86a4c[m Correct bad syntax
[33mde9c86c[m Removed unused files
[33mfd0c50a[m Template for docker deployment jobs
[33m280b7c9[m Made template for deployer deployment
[33m4950cd6[m Testing
[33m5c534d7[m Kubernetes jobs all in one file
[33mde30666[m Corrected deployment error
[33md85e82b[m Testing
[33m5d95c74[m Testing
[33m023d30a[m Testing
[33mfd58fd1[m Fixing creation of too many jobs
[33m48d5cd5[m Fix import
[33mc3f483b[m Try partials
[33m70fdd40[m Use local includes
[33mc8600bf[m Fix syntac
[33m65fda38[m Fix syntax
[33md9567a0[m Removed duplicated variables
[33m800463c[m Added more documentation
[33ma86064a[m More rules on new jobs
[33m5236340[m Moved new files to right folder
[33m6512eee[m Created jobs for deployment through deployer
[33m7aa5e42[m Added rules for job creations
[33m1145a68[m Testing
[33mafa9b04[m Testing
[33m6bcc238[m Testing conditional creation of jobs
[33m41d0fbd[m Added conditions to kube deployment
[33m9c4b913[m Modified default nano laravel template to include files from folders
[33mb4d87be[m Organize files in folders
[33m1f8eef4[m More docs
[33me1adf3f[m More documentation
[33mec15989[m More changes
[33m7a3721a[m Added some documentation for current variables
[33m4edf001[m Merge branch 'master' of git.nano.rocks:nano-images/pipelines-template
[33m0f904c0[m Yaml lint
[33m236ec70[m No needs for npm audit job
[33m120e769[m TEst yaml
[33mae2175e[m Added documentation for variables needed to run the jobs
[33m94c1223[m Changed kube name standards
[33mc14309d[m Better nested templated
[33m456dd2a[m Renamed default template
[33mb9458e8[m Fixed naming mismatch
[33m1652bce[m Fixed name mismatch
[33mf479d78[m Added composer dep to tests jobs
[33m36dea54[m Add dependency for composer
[33me7f28cc[m Tests jobs are failing
[33mca0f760[m Added docker deployment jobs
[33me06796e[m One more
[33mc995d81[m More jobs
[33m40a44f0[m More jobs
[33m4fe4693[m Modified rules for codestyle job
[33mbcdc228[m Added codestyle
[33m56c1bc2[m Changed name
[33mfa5df1b[m More jobs
[33mde270ef[m Changed needs for deploy_image
[33md5bfd44[m Added npm job
[33m9fa3748[m Added job for composer
[33m1138aa1[m Added empty files for all jobs
[33m7c06254[m Changed kube jobs
[33mc4cf188[m Correct syntax
[33mb73da16[m Added kubernetes setup
[33m00c4f6e[m Added docker image and kube deploy templates
